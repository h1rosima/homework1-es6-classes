class Employee {
    #name;
    #age;
    #salary;
    constructor(name, age, salary) {
        this.#name = name;
        this.#age = age;
        this.#salary = salary;
    }

    getName() {
        return this.#name;
    }

    getAge() {
        return this.#age;
    }

    getSalary() {
        return this.#salary;
    }

    setName(name) {
        this.#name = name;
    }

    setAge(age) {
        this.#age = age;
    }

    setSalary(salary) {
        this.#salary = salary;
    }
}

class Programmer extends Employee {
    #lang;
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.#lang = lang;
    }
    getSalary() {
        return super.getSalary() * 3;
    }

    setSalary(salary) {
        super.setSalary(salary);
    }

    getLang() {
        return this.#lang;
    }

    setLang(lang) {
        this.#lang = lang;
    }
}

const firstEmployee = new Programmer('Anton', 19, 2000, 'Java Script');
const secondEmployee = new Programmer('Brandon', 36, 2400, 'Python');
const thirdEmployee = new Programmer('Alex', 34, 3600, 'Java');
console.log(firstEmployee, secondEmployee, thirdEmployee);

//first
console.log(firstEmployee.getName());
console.log(firstEmployee.getAge());
console.log(firstEmployee.getSalary());
console.log(firstEmployee.getLang());
//second
console.log('');
console.log(secondEmployee.getName());
console.log(secondEmployee.getAge());
console.log(secondEmployee.getSalary());
console.log(secondEmployee.getLang());
//third
console.log('');
console.log(thirdEmployee.getName());
console.log(thirdEmployee.getAge());
console.log(thirdEmployee.getSalary());
console.log(thirdEmployee.getLang());
